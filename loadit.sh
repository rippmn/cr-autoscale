#!/bin/bash
export URL=$(gcloud run services describe load-svc --platform managed --region us-central1 --format json | jq -r .status.url)
  i=2

while [ $i -lt 5 ]
do
  gcloud beta compute instances create-with-container load$i --zone=us-central1-f --machine-type=e2-medium --subnet=default --boot-disk-size=10GB --boot-disk-type=pd-balanced --container-image=rippmn/siege --container-restart-policy=always --container-command=siege --container-arg=${URL}
  echo $(date)
  ((i++))
  sleep 120
done

