#!/bin/bash
gcloud services enable compute.googleapis.com run.googleapis.com
PROJECT=$(gcloud config get-value project)
docker pull rippmn/hello-bg-app:0.1
docker tag rippmn/hello-bg-app:0.1 us.gcr.io/${PROJECT}/hello-bg-app:0.1
docker push us.gcr.io/${PROJECT}/hello-bg-app:0.1

gcloud run deploy load-svc --platform managed --region us-central1 --concurrency 20 --image us.gcr.io/${PROJECT}/hello-bg-app:0.1 --allow-unauthenticated

export URL=$(gcloud run services describe load-svc --platform managed --region us-central1 --format json | jq -r .status.url)
i=1
  gcloud beta compute instances create-with-container load$i --zone=us-central1-f --machine-type=e2-medium --subnet=default --boot-disk-size=10GB --boot-disk-type=pd-balanced --container-image=rippmn/siege --container-restart-policy=always --container-command=siege --container-arg=${URL}
